<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactReceived;
use App\Mail\InformationRequest;

use Mail;

class ContactController extends Controller
{
    function SendContact(Request $request){
        $email = 'francisco@ilacoaching.com';
        Mail::to($email)->send(new ContactReceived($request->all()));
        return response()->json([
            "success" => true
        ]);
    }

    function SendInformation(Request $request){
        $email = 'francisco@ilacoaching.com';
        Mail::to($email)->send(new InformationRequest($request->all()));
        return response()->json([
            "success" => true
        ]);
    }
   
}
