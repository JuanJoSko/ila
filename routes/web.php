<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function() { return Redirect::to("/home"); });

Route::get('/', function () {
    return view('index');
})->name('home');

Route::get('/experience', function () {
    return view('experience');
})->name('experience');

Route::get('/school', function () {
    return view('school');
})->name('school');

Route::get('/consultancy', function () {
    return view('consultancy');
})->name('consultancy');



Route::group(['prefix' => 'contact-us'], function() {
    Route::get('/', function () {
        return view('contact');
    })->name('contact');

    Route::post('/sendContact', 'ContactController@SendContact')->name('contactMail');
    Route::post('/sendInformation', 'ContactController@SendInformation')->name('informationMail');


});

Route::group(['prefix' => 'blog'], function() {
    Route::get('/', 'BlogController@index')->name('blog');
    Route::get('/el-rol-de-la-mujer-en-la-sobrevivencia-humana', 'BlogController@show')->name('entry0');
    Route::get('/neurogenero-y-Liderazgo', 'BlogController@show1')->name('entry1');
});





