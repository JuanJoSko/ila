<?php


/*
|--------------------------------------------------------------------------
| User Routes
|--------------------------------------------------------------------------
|
| Here is where you can register user routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "chilemcop" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('landing.chilemcop.index');
});


    
