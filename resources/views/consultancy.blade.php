@extends('layouts.app')

@section('scripts')

    <script>
        var open = false;

        $('.fix-size-consultancy').click(function(){
            console.log('hi');
            var selected = $(this).find('.consult-icon');
            var infoSelected = $(this).find('.consult-icon-info');
            var dom_selected = selected[0];
            var dom_infoSelected = infoSelected[0];
            if(!open){
                animateCSS( dom_selected, 'flipOutY', function() {
                    selected.toggleClass('d-none');
                    infoSelected.toggleClass('d-none');
        
                    animateCSS(dom_infoSelected, 'flipInY');
                    open = true;

                });
                
            }else{
                console.log('revert');

                animateCSS( dom_infoSelected , 'flipOutY', function() {
                    console.log('revert');
                    selected.toggleClass('d-none');
                    infoSelected.toggleClass('d-none');
        
                    animateCSS(dom_selected, 'flipInY');
                });
            }
           

        });

        function animateCSS(element, animationName, callback) {
            const node = element;
            node.classList.add('animated', animationName)
        
            function handleAnimationEnd() {
                node.classList.remove('animated', animationName)
                node.removeEventListener('animationend', handleAnimationEnd)
        
                if (typeof callback === 'function') callback()
            }
        
            node.addEventListener('animationend', handleAnimationEnd)
        }
    </script>
    
@endsection

@section('content')
    <section class="flat-row v11 sec-course-style2 bg-gray fix-top">
        <div class="container">
            <div class="row mb-1">
                <div class="col-md-12">
                    <div class="center">
                        <h5 class="title-info title-red mb-1">LA CONSULTORIA</h5>
                    </div>
                    <div class="center">
                        <p class="info-banner">
                            Abordamos nuestro trabajo en cinco áreas específicas <br>
                            que proponemos atender
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 section-reponsive fix-size-consultancy ">
                    <article class="post style3 consult-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon_cons_1 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Dirección</span>
                                </div>
                            </div>
                        </div>
                    </article>
                    <article class="post style3 consult-icon-info first clearfix d-none">
                        <div class="entry">
                            <span class="icon-icon_cons_1 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Dirección</span>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 section-reponsive fix-size-consultancy">
                    <article class="post style3 consult-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon_cons_5 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Interacción</span>
                                </div>
                            </div>
                        </div> 
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 section-reponsive fix-size-consultancy">
                    <article class="post style3 consult-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon_cons_2 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Renovación</span>
                                </div>
                            </div>
                        </div> 
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 section-reponsive fix-size-consultancy">
                    <article class="post style3 consult-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon_cons_3 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Ajuste sistémico</span>
                                </div>
                            </div>
                        </div> 
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 section-reponsive fix-size-consultancy">
                    <article class="post style3 consult-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon_cons_4 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Bienestar</span>
                                </div>
                            </div>
                        </div> 
                    </article>
                </div>
            <div class="divider h50"></div>  
            </div>
        </div>
    </section>
    <section class="flat-row v5 sec-teammember">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section center">
                        <h5 class="title-info title-red mb-1">Nuestro Modelo de CONSULTORIA ILA</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="team-content">
                <div class="row">
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <p class="position">Individualidad</p>
                        <br>
                        <p>
                            Reconocemos la individualidad de cada
                            participante en la organización y, por ende, su
                            modo de actuar. Cada integrante tiene una
                            manera particular de observar y, de esta
                            manera, de generar acciones al interior de una
                            organización y de influir con otros desde
                            su propia mirada.
                        </p>
                    </div>
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <p class="position">Coordinación</p>
                        <br>
                        <p>
                            Planteamos que la coordinación de acciones
                            de quienes participan en una organización,
                            se consolida como una actividad central y
                            estratégica para el desempeño eficiente de la
                            empresa. Es un espacio importante y habitual
                            para el fortalecimiento.
                        </p>
                    </div>
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <p class="position">Visualización</p>
                        <br>
                        <p>
                            Proponemos un Lenguaje basado en el futuro.
                            Realizar indagación apreciativa en el
                            presente, con el fin de fundar un futuro en
                            común, innovador y transformacional, moviliza
                            a la organización en dirección a un total
                            alineamiento organizacional y a la mejora
                            sustantiva en el desempeño de la misma
                        </p>
                    </div>
                </div>
                <div class="row fix-top">
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <p class="position">Aprendizaje</p>
                        <br>
                        <p>
                            Promovemos la vocación hacia el aprendizaje.
                            Una unidad (trabajador o equipo) que
                            clausura su aprendizaje ve limitada su
                            capacidad de expandir sus oportunidades de
                            desarrollo. En muchas ocasiones, los equipos
                            en las organizaciones, necesitan ayuda
                            para observar aquello que es necesario
                            cambiar y revisar sus procesos de aprendizaje.
                        </p>
                    </div>
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <p class="position">Emprendimiento</p>
                        <br>
                        <p>
                            Aprender para emprender. Proponemos que el
                            aprendizaje es la manera que tiene el ser
                            humano de transformarse a sí mismo, y una
                            vez logrado esto, puede transformar su
                            entorno desde el emprendimiento.
                            Sostenemos que el aprendizaje es experiencia,
                            el resto es información
                        </p>
                    </div>
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <p class="position">Comunicación</p>
                        <br>
                        <p>
                            La comunicación efectiva es la llave maestra.
                            Estamos convencidos que las competencias
                            técnicas funcionales que cada individuo tiene
                            al interior de la organización se ven fortalecidas
                            con esta herramienta. El compartir
                            inquietudes, coordinar acciones y entablar
                            lineamientos explícitos y/o tácitos de
                            comunicación efectiva garantiza el alto
                            desempeño de la organización.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="flat-row v20 bg-theme">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section center oregano">
                        <h5 class="title-info title-red mb-1">NUESTRA CONSULTORIA ESPECIALIZADA</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="blog-carousel">
                <div class="post-shortcode">
                    <article class="post style3 lates first clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_tic.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">Desarrollo de equipos</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates second clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_cead.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">Construcción de equipos  <br>de alto desempeño</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates three clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_dm.jpg') }}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">Competencias genéricas de <br>programas de formación in company</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates three clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_pe.jpg') }}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">Executive Coaching</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates first clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_ge.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">Carrera de aventura </a></h6>
                            </div>
                        </div> 
                    </article>
                </div>
            </div>
            <div class="row fix-top">
                <div class="blog-carousel">
                    <div class="post-shortcode">
                        
                        <article class="post style3 lates second clearfix">
                            <div class="entry">
                                <div class="featured-post">
                                    <a href="#"><img src="{{asset('img/esc_pp.jpg')}}" alt="image"></a>
                                </div><!-- /.feature-post -->
                                <div class="entry-post">
                                    <h6 class="entry-title"><a href="#">Evaluación y valorización <br>del recurso humano</a></h6>
                                </div>
                            </div> 
                        </article>
                        <article class="post style3 lates three clearfix">
                            <div class="entry">
                                <div class="featured-post">
                                    <a href="#"><img src="{{asset('img/esc_spin.jpg') }}" alt="image"></a>
                                </div><!-- /.feature-post -->
                                <div class="entry-post">
                                    <h6 class="entry-title"><a href="#">Medición y gestión <br> de clima organizacional</a></h6>
                                </div>
                            </div> 
                        </article>
                        <article class="post style3 lates three clearfix">
                            <div class="entry">
                                <div class="featured-post">
                                    <a href="#"><img src="{{asset('img/esc_nm.jpg') }}" alt="image"></a>
                                </div><!-- /.feature-post -->
                                <div class="entry-post">
                                    <h6 class="entry-title"><a href="#">Diagnostico de <br> estadios de equipos</a></h6>
                                </div>
                            </div> 
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

    {{-- <section class="v19">
        <div class="row style-ove fix-top">
            <div class="col-md-4 section-reponsive col-sm-6">
                <div class="feature-about-us">
                    <img src="{{ asset('/img/coa_desarrollo_2.jpg') }}">
                </div>
            </div>
            <div class="col-md-7 section-reponsive col-sm-6 fix-experience">
                <div class="divider ">
                </div>
                <div class="col-md-12 fix-top">
                    <h5 class="expe-subt">Desarrollo de equipos </h5>
                    <p class=" mb-1">
                        Un proceso que incluye: Conciencia, Integración, Formación y
                        Desarrollo de equipos.
                    </p>
                    <div class="clearfix"></div>
                    <div class="">
                        <br>
                        <p>
                            • Integración y alineación en torno a objetivos
                        </p>
                        <p>
                            • Consolidación de roles y liderazgos
                        </p>
                        <p>
                            • Capacitación en herramientas específicas
                        </p>
                        <p>
                            •  Manejo y aprovechamiento consciente de las interacciones
                            para generar resultados excepcionales en el alcance de los objetivos
                        </p>
                    </div>
                   <div class="">
                        <div class="divider "></div>
                        <h5 class="title-info title-red mb-1">Solicitar más información</h5>
                        <form class="info-form" method="POST" action="http://trabajoexpress.loc/login">
                            <input type="hidden" name="_token" value="0UiuU8SQhYaIigmB9fF966Xt3rcJWdwlXiFppjcB">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input placeholder="Nombre completo" type="text" class="form-control" name="name" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input placeholder="Correo electónico" type="email" class="form-control" name="email" >
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-4">
                                    <button class="btn flat-button" type="submit">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
