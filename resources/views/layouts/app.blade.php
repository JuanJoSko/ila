<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta id="tokenID" name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" type="image/jpg" href="{{ asset('/img/logo_menu.png') }}"/>

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}" >
    <link rel="stylesheet" type="text/css" href="{{ asset('css/modal.css') }}" >
    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/settings.css') }}">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/colors/color4.css') }}" id="colors">
    
    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">

    <!-- Favicon and touch icons  -->
    <link href="{{ asset('icon/logo_menu.png') }}" rel="shortcut icon">

    <link rel="stylesheet" href="{{ asset('/icomoon/style.css') }}">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">



</head>
    <body class="bg-porto">
        <div class="page-wrapper"> 
            @include('static.header')

            @yield('content')
            @include('static.pop_up')
            <div class="asagiSabit whatsappBlock">
                <a href="https://web.whatsapp.com/send?phone=+5521868492" target="_blank">
                    Whatsapp Live Chat
                </a>
            </div>
            @include('static.footer')

        </div>
        <!-- Javascript -->
        <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('js/jquery.easing.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('js/jquery-waypoints.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('js/owl.carousel.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('js/jquery-validate.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('js/jquery.event.move.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.twentytwenty.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/jquery.cookie.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/jquery-countTo.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/parallax.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>   
        
        <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>

        <!-- Revolution Slider -->
        <script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.tools.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/slider.js') }}"></script>

        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->    
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>

        @yield('scripts')

        <script>
                $( document ).ready(function() {
                    var pathCurrent = window.location.href;
                    $('.nav-link').each(function (index, value) {
                        if($(this).attr('href')+'/' == window.location.href){
                             $(this).parent('li').addClass('active');
                        }
                        if($(this).attr('href') == window.location.href){
                             $(this).parent('li').addClass('active');
                        }
                     });
                     $('#dontShow').click(function(){
                        localStorage['showPopup'] = false;
                     });
                        var activePop = localStorage['showPopup'];

                        if(activePop == 'false')
                            $('#popupInfo').modal('hide');
                        else
                            $('#popupInfo').modal('show');


                });

                $(window).bind('scroll', function () {
                    if ($(window).scrollTop() > 50) {
                        $('.header').addClass('fixed');
                    } else {
                        $('.header').removeClass('fixed');
                    }
                });
                    
        </script>
        
    </body>
</html>
