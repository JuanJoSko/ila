<div id="rev_slider_1078_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container home-health-coach" data-alias="classic4export" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
    <!-- START REVOLUTION SLIDER 5.3.0.2 auto mode -->
    <div id="rev_slider_1078_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
        <div class="slotholder"></div>
        <ul><!-- SLIDE  -->
    
            

            <!-- SLIDE 2 -->
             <!-- SLIDE 2 -->
             <li data-index="rs-3048" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="500" data-duration="0"   data-rotate="0"  data-saveperformance="off"  data-title="HTML5 Video" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                <!-- MAIN IMAGE -->
                <img src="{{asset('img/banner_35.jpg')}}"  alt=""  data-bgposition="center center" data-kenburns="off" data-duration="0" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>


                <!-- LAYER NR. 12 -->
                <div class="tp-caption title-slide" 
                    id="slide-3049-layer-1" 
                    data-x="center" 
                    data-hoffset="0" 
                    data-y="['middle','middle','middle','middle']" data-voffset="['-102','-102','-50','-50']" 
                    data-fontsize="['60','60','45','30']"
                    data-lineheight="['65','65','45','35']"
                    data-fontweight="['300','300','300','300']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
            
                    data-type="text" 
                    data-responsive_offset="on"                             

                    data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                    data-textAlign="center"
                    data-paddingtop="[10,10,10,10]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 16; white-space: nowrap;text-transform:left;">IMPLEMENTA LA NORMA 35 EN TU EMPRESA <br> NOSOTROS TE ASESORAMOS
                </div>

                 <!-- LAYER NR. 13 -->

                 <a 
                    href="{{ route('contact') }}"
                    target="_self"
                    data-fontsize="20"
                    class="tp-caption flat-button-slider flat-button color-green btn-slide2"         
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                    data-x="['center','center','center','center']"
                    data-hoffset="0" 
                    data-y="['middle','middle','middle','middle']"
                    data-voffset="160" 
                    data-width="auto"
                    data-height="['auto']">CONTACTANOS
                </a>

            </li>

            <!-- SLIDE 1 -->
            <li data-index="rs-3049" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"    data-rotate="0"  data-saveperformance="off"  data-title="Ken Burns" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">                        

                <!-- MAIN IMAGE -->
                <img src="{{asset('img/home_1.jpg')}}"  alt=""  data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 12 -->
                <div class="tp-caption title-slide" 
                    id="slide-3049-layer-1" 
                    data-x="center" 
                    data-hoffset="0" 
                    data-y="['middle','middle','middle','middle']" data-voffset="['-102','-102','-50','-50']" 
                    data-fontsize="['60','60','45','30']"
                    data-lineheight="['65','65','45','35']"
                    data-fontweight="['300','300','300','300']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
            
                    data-type="text" 
                    data-responsive_offset="on"                             

                    data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

                    data-textAlign="center"
                    data-paddingtop="[10,10,10,10]"
                    data-paddingright="[0,0,0,0]"
                    data-paddingbottom="[0,0,0,0"
                    data-paddingleft="[0,0,0,0]"

                    style="z-index: 16; white-space: nowrap;text-transform:left;">CAMBIA VERDADES ABSOLUTAS <br> POR INTERPRETACIONES PODEROSAS
                </div>

                <a 
                    href="#"
                    target="_self"
                    data-fontsize="20"
                    class="tp-caption flat-button-slider flat-button color-green btn-slide2"         
                    data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                    data-x="['center','center','center','center']"
                    data-hoffset="0" 
                    data-y="['middle','middle','middle','middle']"
                    data-voffset="160" 
                    data-width="auto"
                    data-height="['auto']">Conoce nuestra metodología experimental
                </a>

            </li>

        </ul>
    </div>
</div><!-- END REVOLUTION SLIDER -->