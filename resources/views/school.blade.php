@extends('layouts.app')

@section('content')
    <section class="v19">
        <div class="row style-ove">
            <div class="col-md-5 section-reponsive col-sm-6">
                <div class="feature-about-us">
                    <img src="{{ asset('/img/escuela.jpg') }}">
                </div>
            </div>
            <div class="col-md-6 section-reponsive col-sm-6 fix-experience fix-top">
                <div class="divider ">
                </div>
                <div class="col-md-8 fix-top-10">
                    <h5 class="title-info title-red mb-1">LA ESCUELA</h5>
                    <p class=" mb-1">
                        Creemos firmemente que el principal desafío actual para los seres
                        humanos está siendo la velocidad con la que se están presentando los
                        cambios. Cambios en la geografía, en los recursos, en los modos de
                        convivencia, en la esperanza de vida, entre otros, para los cuales no
                        estábamos preparados. Esto nos propone una
                        invitación.
                    </p>

                    <p>
                        La invitación es a aceptarnos como seres cambiamos y hacer algo por
                        ello. La escuela de ILA coaching y consultoría nace como un espacio
                        académico de excelencia que busca acompañar al ser humano actual
                        en su constante y necesaria transformación personal y profesional.
                    </p>
                    <p>
                        La escuela tiene dos ejes de programas de formación en el ámbito
                        personal y que también pueden ser puestos en marcha en
                    </p>
                </div>
            </div>
           
        </div>
    </section>
    <section class="flat-row v20 bg-theme">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section center oregano">
                        <h5 class="title-info title-red mb-1">Programas de Formación o Diplomados <br>
                                (in Company o abiertos)</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="blog-carousel">
                <div class="post-shortcode">
                    <article class="post style3 lates first clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_cop.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">COP: Coach Ontológico <br>Psicodinámico</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates second clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_cde.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">CDE: Competencias Directivas <br>Estratégicas</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates three clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_cte.jpg') }}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">CTE: Competencias Transversales <br> para Educación</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates three clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_mpco.jpg') }}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">MPCO: Mención psicogénica <br>para coaches ontológicos</a></h6>
                            </div>
                        </div> 
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section class="flat-row v20 bg-theme">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section center oregano">
                        <h5 class="title-info title-red mb-1">Cursos especializados</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="blog-carousel">
                <div class="post-shortcode">
                    <article class="post style3 lates first clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_tic.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#"> TIC: Técnicas de intervención <br> en Coaching</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates second clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_cead.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">CEAD: Construcción de equipos <br>de alto desempeño</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates three clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_dm.jpg') }}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">DM: Digital Mindset</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates three clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_pe.jpg') }}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#">PE: Presentaciones Efectivas</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates first clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="#"><img src="{{asset('img/esc_ge.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="#"> GE: Gestión Educational</a></h6>
                            </div>
                        </div> 
                    </article>
                </div>
            </div>
            <div class="row fix-top">
                <div class="blog-carousel">
                    <div class="post-shortcode">
                        
                        <article class="post style3 lates second clearfix">
                            <div class="entry">
                                <div class="featured-post">
                                    <a href="#"><img src="{{asset('img/esc_pp.jpg')}}" alt="image"></a>
                                </div><!-- /.feature-post -->
                                <div class="entry-post">
                                    <h6 class="entry-title"><a href="#">PP: Pensamiento <br>productivo/Innovación</a></h6>
                                </div>
                            </div> 
                        </article>
                        <article class="post style3 lates three clearfix">
                            <div class="entry">
                                <div class="featured-post">
                                    <a href="#"><img src="{{asset('img/esc_spin.jpg') }}" alt="image"></a>
                                </div><!-- /.feature-post -->
                                <div class="entry-post">
                                    <h6 class="entry-title"><a href="#">SPIN</a></h6>
                                </div>
                            </div> 
                        </article>
                        <article class="post style3 lates three clearfix">
                            <div class="entry">
                                <div class="featured-post">
                                    <a href="#"><img src="{{asset('img/esc_nm.jpg') }}" alt="image"></a>
                                </div><!-- /.feature-post -->
                                <div class="entry-post">
                                    <h6 class="entry-title"><a href="#">NM: Neuromanagement</a></h6>
                                </div>
                            </div> 
                        </article>
                        <article class="post style3 lates three clearfix">
                            <div class="entry">
                                <div class="featured-post">
                                    <a href="#"><img src="{{asset('img/esc_fe.jpg') }}" alt="image"></a>
                                </div><!-- /.feature-post -->
                                <div class="entry-post">
                                    <h6 class="entry-title"><a href="#">FE: Facilitación Experiencial</a></h6>
                                </div>
                            </div> 
                        </article>
                        <article class="post style3 lates three clearfix">
                            <div class="entry">
                                <div class="featured-post">
                                    <a href="#"><img src="{{asset('img/esc_imagine.jpg') }}" alt="image"></a>
                                </div><!-- /.feature-post -->
                                <div class="entry-post">
                                    <h6 class="entry-title"><a href="#">IMAGINE: Entrepeneur Culture</a></h6>
                                </div>
                            </div> 
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
    {{-- <section class="v19">
        <div class="row style-ove fix-top">
            <div class="col-md-4 section-reponsive col-sm-6">
                <div class="feature-about-us">
                    <img src="{{ asset('/img/esc_cop_2.jpg') }}">
                </div>
            </div>
            <div class="col-md-7 section-reponsive col-sm-6 fix-experience">
                <div class="divider ">
                </div>
                <div class="col-md-12 fix-top">
                    <h5 class="title-info title-red mb-1">Coach Ontológico Psicodinámico (COP)</h5>
                    <p class=" mb-1">
                        La propuesta del coaching ontológico psicodinámico surge como
                        disciplina técnica al servicio de la validación de las diferencias, del
                        reconocimiento de estructuras personales y organizacionales que
                        necesitan declararse abiertas para aprovechar su inmenso poder de
                        transformación.
                    </p>

                    <ul>
                        <li>
                            <div class="school-icons-content">
                                <span class="icon-icon_teatro_1 school-icons"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                <span>Práctica de coaching</span>
                            </div>
                        </li>
                        <li>
                            <div class="school-icons-content">
                                <span class="icon-icon_teatro_2 school-icons"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                <span>Laboratorios</span>
                            </div>
                        </li>
                        <li>
                            <div class="school-icons-content">
                                <span class="icon-icon_teatro_3 school-icons"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                <span>Talleres</span>
                            </div>
                        </li>
                        <li>
                            <div class="school-icons-content">
                                <span class="icon-icon_teatro_4 school-icons"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                <span>Sesiones de transferencia</span>
                            </div>
                        </li>
                        <li>
                            <div class="school-icons-content">
                                <span class="icon-icon_teatro_5 school-icons"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                <span>Examen público</span>
                            </div>
                        </li>
                        <li>
                            <div class="school-icons-content">
                                <span class="icon-icon_teatro_5 school-icons"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                <span>Coaching personal</span>
                            </div>
                        </li>
                        <li>
                            <div class="school-icons-content">
                                <span class="icon-icon_teatro_5 school-icons"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                <span>Trabajos de asimilación</span>
                            </div>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <h5 class="school-title">Dentro del programa</h5>
                    <div class="">
                        <span class="expe-subt">Fundamentos EPISTEMOLOGICOS</span>
                        <br>
                        <p>
                            • Principios Ontológicos y la Ontología del Lenguaje** 
                        </p>
                        <p>
                            • Propuesta Psicodinamica 
                        </p>
                        <p>
                            • La Escucha efectiva 
                        </p>
                        <p>
                            • Freud, Lacan y Jung 
                        </p>
                    </div>
                    <div class="">
                        <span class="expe-subt">Nuestra Propuesta De Coaching Ontológico Psicodinamico</span>
                        <br>
                        <p>
                            • El Observador y sus dominios 
                        </p>
                        <p>
                            • La Personalidad y sus registros 
                        </p>
                        <p>
                            • La Gestion Emocional 
                        </p>
                        <p>
                            • El Reconocimiento de la Corporalidad 
                        </p>
                        <p>
                            • Aprendizaje y Modelo OSARE 
                        </p>
                        <p>
                            • Herramientas principales del Coach. 
                        </p>
                        <p>
                            • Fases del coaching y modelo COP ILA. 
                        </p>
                        <p>
                            • Educación Experiencial y el ciclo experiencias. 
                        </p>
                        <p>
                            • El nuevo reto de las Conversaciones. 
                        </p>
                    </div>
                    <div class="">
                        <span class="expe-subt">Profundización Organizacional</span>
                        <br>
                        <p>
                            • El Coaching en los equipos y en las organizaciones. 
                        </p>
                        <p>
                            • Modelos de Liderazgo y Gestión Empresarial 
                        </p>
                        <p>
                            • La Retroalimentación, la gestión emocional y la cultura de impecabilidad en las
                            organizaciones.                        
                        </p>
                        <p>
                            • El nuevo Líder y las Competencias de Gestión. 
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="v19">
        <div class="row style-ove">
            <div class="col-md-4 section-reponsive col-sm-6 pl-1">
                <div class="divider "></div>
                <h5 class="title-info title-red mb-1">Solicitar más información</h5>
                <form class="info-form" method="POST" action="http://trabajoexpress.loc/login">
                    <input type="hidden" name="_token" value="0UiuU8SQhYaIigmB9fF966Xt3rcJWdwlXiFppjcB">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input placeholder="Nombre completo" type="text" class="form-control" name="name" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <input placeholder="Correo electónico" type="email" class="form-control" name="email" >
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-12 offset-md-4">
                            <button class="btn flat-button" type="submit">ENVIAR</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-7 section-reponsive col-sm-6 pl-1">
                <h5 class="school-title">Duración del programa</h5>
                <span class="expe-subt">8 meses</span>
            </div>
            
        </div>
    </section> --}}
    
