@extends('layouts.app')

@section('content')
    <div id="services-section" class="site-blocks-cover overlay aos-init aos-animate" style="background-image: url({{ asset('/img/background_experience.jpg') }});" data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-lg-5 ml-auto text-left align-self-center align-self-md-center">
                    <div class="col-md-6">
                        <div class="experience-banner">
                            <h5 class="title-info title-red mb-1">LO QUE NOS UNE</h5>
                            <p>
                                Creemos en la necesidad de construir un mundo
                                participativo e incluyente, basado en el respeto y
                                el empoderamiento estratégico de las diferencias
                                individuales y colectivas.
                            </p>
                            <br>
                            <p>
                                Pensamos que el camino para la evolución y la
                                supervivencia humana está en la apertura al
                                cambio constante a través del aprendizaje
                                continuo.
                            </p>
                            <p>
                                Proponemos cambiar verdades absolutas por
                                interpretaciones poderosas.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="{{ asset('/img/experience_00.jpg') }}" alt="">
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <section class="flat-row v11 sec-course-style2 bg-gray">
        <div class="container">
            <div class="row mb-1">
                <div class="col-md-12">
                    <div class="center">
                        <h5 class="title-info title-red mb-1">NUESTRA METODOLOGIA ( M.E.T.A)</h5>
                    </div>
                    <div class="center">
                        <p class="info-banner">
                            Con el fin de impactar a través de los servicios a los cuales esta comprometida ILA,
                            hemos desarrollado una metodología propia que nos permite atender los
                            requerimientos de nuestros clientes personales y organizacionales 
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 section-reponsive">
                    <article class="post style3 method-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon_meto_1 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Movilizadora</span>
                                </div>
                                <p class="entry-title">
                                    Proponemos una metodología basada en retos lúdicos que impacten positivamente en la emocionalidad de los participantes. De este modo motivarlos y llevarlos a la expansión de su apertura frente al aprendizaje
                                </p>
                            </div>
                        </div> 
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 section-reponsive">
                    <article class="post style3 method-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon-05 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Experiencial</span>
                                </div>
                                <p class="entry-title">
                                    En lugar de presentar información a manera de expositores/expertos, creamos situaciones que invitan a los participantes a descubrir sus propias respuestas a asuntos relacionados con la necesidad de intervención.
                                </p>
                                
                            </div>
                        </div> 
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 section-reponsive">
                    <article class="post style3 method-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon-07 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Transformacional</span>
                                </div>
                                <p class="entry-title">
                                    Diseñamos y operamos nuestros programas promoviendo el “awareness” de los participantes en relación a los aprendizajes necesarios personales y colectivos para el incremento sustancial del desempeño.
                                </p>
                            </div>
                        </div> 
                    </article>
                </div>
                <div class="col-md-3 col-sm-6 section-reponsive">
                    <article class="post style3 method-icon first clearfix">
                        <div class="entry">
                            <span class="icon-icon-09 orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                            <div class="entry-post">
                                <div class="entry-author">
                                    <span class="name-author">Aplicada</span>
                                </div>
                                <p class="entry-title">
                                    Proponemos un “Debriefing
                                    Coach” por cada 10 participantes
                                    para lograr la aplicación concreta
                                    al día a día en el quehacer laboral
                                    o personal de los participantes.
                                </p>
                            </div>
                        </div> 
                    </article>
                </div>
            </div>
            <div class="divider h50"></div>   
        </div>
    </section>
        

    <section class="flat-row v19">
            <div class="row style-ove">
                <div class="col-md-5 section-reponsive col-sm-6">
                    <div class="feature-about-us">
                        <img src="{{ asset('/img/exp_2.jpg') }}">
                    </div>
                </div>
                <div class="col-md-7 section-reponsive col-sm-6">
                    <div class="divider ">
                    </div>
                    <div class="profile col-md-10">
                        <h5 class="title-info title-red mb-1">NUESTRAS EXPERIENCIAS</h5>
                        
                        <div >
                            <p>
                                Una de nuestras posibilidades en experiencias es ofrecerle a las personas y a las
                                organizaciones, un camino de transformación que les permita mejorar sus resultados,
                                ampliar sus posibilidades, mejorar su clima y convivencia, a través del arte. Para ellos
                                proponemos un novedoso enfoque de intervenir organizaciones basado en dos
                                conceptos: Eduentretención y Divertizaje.
                            </p>
                            <br>
                            <p>
                                “El éxito y la felicidad de un individuo en sociedad y en las organizaciones de la que
                                forma parte se fundamenta, además de las competencias técnicas funcionales en las
                                que este ha sido formado, en la capacidad de adquirir competencias transversales
                                tales como: Liderazgo, Espíritu emprendedor, Ética, compromiso social, Comunicación
                                efectiva, Innovación, Creatividad, Pensamiento crítico, Solución de problemas y trabajo
                                en equipo. Estas son fundamentales para el desarrollo del propio individuo y de las
                                organizaciones”
                            </p>
                            <br>
                            <p>
                                ILA, propone una metodología de capacitación a través del teatro con el objetivo que
                                los participantes reflexionen acerca de habilidades y competencias fundamentales
                                para la efectividad organizaciona lo grupal, desde la experiencia teatral como
                                espectador de montajes de textos clásicos y un procesamiento de sus temáticas
                                desagregando aquellos factores, habilidades, competencias que se requieren instalar,
                                mejorar u optimizar.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <section class="flat-row v11 sec-course-style2 bg-gray">
        <div class="container">
            <div class="row mb-1">
                <div class="col-md-12">
                    <div class="center">
                        <h5 class="title-info title-red">NUESTROS MONTAJES</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-6 section-reponsive">
                    <div class="post style3 method-montahe first clearfix">
                        <div class="col-md-6 col-sm-6 section-reponsive">
                            <div class="col-md-4 col-sm-6 section-reponsive method-fix">
                                <span class="icon-icon_school orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                                <div class="entry-author">
                                    <span class="name-author method-title">EN ESCUELAS</span>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-6 section-reponsive">
                                <p>
                                    Selección de obras de la dramaturgia
                                    universal que sean parte de la
                                    programación de la autoridad pertinente
                                    en educación según ciclo escolar.

                                </p>
                                <br>
                                <p>
                                    En México lo dividimos en 4: Pre-primaria,
                                    Primaria, Secundaria yPreparatoria.
                                    Asimismo, contenidos dirigidos según
                                    requerimientos.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 section-reponsive">
                            <div class="col-md-5 col-sm-6 section-reponsive method-fix">
                                <span class="icon-icon_org orange-icon center"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span></span>
                                <div class="entry-author">
                                    <span class="name-author method-title">ORGANIZACIONES</span>
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-6 section-reponsive method-fix">
                                <p>
                                    Selección de obras de la dramaturgia
                                    universal según temáticas a intervenir en
                                    las organizaciones.
                                </p>
                                <br>
                                <p>
                                    A nuestro juicio, los clásicos devienen
                                    clásicos debido a sus contenidos
                                    transversales
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider h50"></div>   
        </div>
    </section>
    <section class="v19">
        <div class="row style-ove">
            <div class="col-md-6 section-reponsive col-sm-6 fix-experience">
                <div class="divider ">
                </div>
                <div class="col-md-12">
                    <h5 class="title-info title-red mb-1">RECURSO EXPERIENCIAL</h5>
                    <span class="expe-subt">Puesta en Escena:</span>
                    <p class=" mb-1">
                        Montaje de la obra seleccionada bajo el sustento conceptual de
                        Eduentretención y Divertizaje, tomando en cuenta 5 factores claves a modo de pauta:
                    </p>

                    <ul class="clear">
                        <li>
                            <div class="experience-points">
                                <span class="icon-icon_teatro_1 experience-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                Adaptación dramatúrgica del texto 
                            </div>
                        </li>
                        <li>
                            <div class="experience-points">
                                <span class="icon-icon_teatro_2 experience-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                Selección Soundtrack unitario
                            </div>
                        </li>
                        <li>
                            <div class="experience-points">
                                <span class="icon-icon_teatro_3 experience-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                Inserción de otras áreas del arte
                                (Danza/Coreografías) 
                            </div>
                        </li>
                        <li>
                            <div class="experience-points">
                                <span class="icon-icon_teatro_4 experience-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                Reparto según casting inmediato 
                            </div>
                        </li>
                        <li>
                            <div class="experience-points">
                                <span class="icon-icon_teatro_5 experience-icon"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span></span>
                                Escucha/Utilización de contexto Actual
                                y necesidades de intervención
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-5 section-reponsive col-sm-6">
                <div class="feature-about-us">
                    <img src="{{ asset('/img/exp_3.jpg') }}">
                </div>
            </div>
        </div>
    </section>
    <section class="v19">
            <div class="row style-ove">
                <div class="col-md-8 section-reponsive col-sm-6">
                    <div class="feature-about-us">
                        <img src="{{ asset('/img/exp_4.jpg') }}">
                    </div>
                </div>
                <div class="col-md-4 section-reponsive col-sm-6 pt-1">
                    <div class="divider "></div>
                    <h5 class="title-info title-red mb-1">Solicitar más información</h5>
                    <p id="alert-contact" class="alert alert-success hidden">Tu mensaje ha sido enviado. <br>Gracias por ponerte en contacto con nosotros.</p>

                    <form id="infoForm" class="info-form" method="POST" action="{{ route('informationMail') }}">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input placeholder="Nombre completo" type="text" class="form-control" name="name" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input placeholder="Correo electónico" type="email" class="form-control" name="email">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <button class="btn flat-button" type="submit">ENVIAR</button>
                            </div>
                        </div>
                    </form>
                </div>
               
            </div>
    </section>
    <section class="row team-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="col-md-6">
                        <img src="{{ asset('/img/team_1.png') }}" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="">
                            <h5 class="title-info title-red mb-1">Francisco Javier Ahumada Rubio</h5>
                            <span class="expe-subt">Facilitador Central ILA Coaching y Consultoría SC</span>
                            <br>
                            <p>
                                • Maestría en Consultoría y Coaching Organizacional:
                                Universidad del Desarrollo.
                            </p>
                            <p>
                                • Coach Ontológico Senior certificado por Newfield Consulting:
                                    Santiago de Chile.
                            </p>
                            <p>
                                • Psicología , Pontificia Universidad Católica de Chile.
                            </p>
                            <p>
                                • Actor, Pontificia Universidad Católica de Chile.
                            </p>
                            <p>
                                • Psicodrama, Universidad de Chile.
                            </p>
                            <p>
                                • Psicoanálisis, Escuela Freudiana de B. A.
                            </p>
                            <p>
                                • Fundador de ILA coaching y consultoría
                            </p>
                            <p>
                                • Fundador de la Escuela Psicogénica.
                            </p>
                            <p>
                                • Autor de la teoría y creador de la técnica del coaching psicogénico
                                u ontológico psicodinámico.
                            </p>
                            <p>
                                • Es facilitador de programas, talleres y conferencias de desarrollo
                                de competencias de coaching, liderazgo, gestión del cambio,
                                psicoanálisis, construcción de equipos, alineación cultural y creación
                                de unidades de alto desempeño.
                            </p>
                        </div>
                    </div>
                   
                </div>
                
            </div>
        </div>
    </section>
    <section class="flat-row v5 sec-teammember">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section center">
                        <h5 class="title-info title-red mb-1">NUESTROS COACHES</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="team-content">
                <div class="row">
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <div class="content">
                            <img src="{{ asset('img/team_4.png') }}" alt="">
                            <h6 class="name">Josefina Labadia Miguel</h6>
                            <p class="position">Directora Escuela ILA Coaching</p>
                        </div>
                        <ul>
                            <li>
                                • Coach Ontológico, certificado por
                                Newfield Consulting Santiago de Chile.
                            </li>
                            <li>
                                • Actuación , Pontificia Universidad
                                Católica de Chile.
                            </li>
                            <li>
                                • Danza, Centro de Formación
                                KarenConolly, Chile
                            </li>
                            <li>
                                • Fundadora de ILA coaching y consultoría
                            </li>
                            <li>
                                
                            </li>
                            <li>
                                • Facilitadora de programas, talleres y
                                conferencias de desarrollo de
                                competencias de Coaching, Gestión
                                emocional y Validación
                                corporal
                            </li>
                            <li>
                                • Consultora y facilitadora Organizacional.
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <div class="content">
                            <img src="{{ asset('img/team_5.png') }}" alt="">
                            <h6 class="name">Mariano Squella Bravo</h6>
                            <p class="position">Representante Comercial ILA Chile</p>
                        </div>
                        <ul>
                            <li>
                                • Coach Ontológico, certificado por
                                Newfield Consulting Chile
                            </li>
                            <li>
                                • Diplomado en entrenamiento para la
                                formación de terapeutas y sanadores
                                desde un abordaje posmoderno
                                narrativo, Chile
                            </li>
                            <li>
                                • Ingeniero Civil de la Universidad de
                                Chile.
                            </li>
                            <li>
                                • Evaluación de Proyectos en el Colegio
                                de Ingenieros.
                            </li>
                            <li>
                                • Líder de equipos de alto desempeño en
                                su trayectoria laboral en los rubros
                                descritos anteriormente
                            </li>
                            <li>
                                • Experiencia en el desarrollo de
                                proyectos de construcción, agrícolas
                                y gastronómicos
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <div class="content">
                            <img src="{{ asset('img/team_6.png') }}" alt="">
                            <h6 class="name">Ana Maria Nader Danies</h6>
                            <p class="position">Consultora Asociada</p>
                        </div>
                        <ul>
                            <li>
                                • Coach Ontológica Psicodinámica
                                certificado por ILA Coaching y
                                Consultoría SC
                            </li>
                            <li>
                                • Certificación en técnica de Alba
                                Emoting
                            </li>
                            <li>
                                • Licenciada en Derecho, Universidad
                                Javeriana Bogotá y Especialista en
                                Legislación Financiera de la
                                Universidadde los Andes Bogotá
                            </li>
                            <li>
                                • Experiencia en el sector Financiero
                                Colombia
                            </li>
                            <li>
                                • Voluntariado en área educacional y
                                psicológica Centro Comunitario
                                Santa Fe
                            </li>
                            <li>
                                • Fundadora y Consultora de Aselink S.A.
                                Colombia
                            </li>
                            <li>
                                • Consultora y facilitadora Organizacional
                                y personal.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row fix-top">
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <div class="content">
                            <img src="{{ asset('img/team_2.png') }}" alt="">
                            <h6 class="name">Mariana Mendoza Tapia</h6>
                            <p class="position">Consultora Asociada</p>
                        </div>
                        <ul>
                            <li>
                                • Coach Ontológico Psicodinámico,
                                certificado por ILA Coaching y
                                Consultoria SC
                            </li>
                            <li>
                                • Licenciada en Administración con
                                especialidad en Recursos Humanos.
                            </li>
                            <li>
                                • Coach ejecutivo, consultora y
                                facilitadora en temas de Desarrollo
                                Organizacional, Estructuras, Gestión
                                de Cambio y Atracción de Talento en
                                empresas nacionales y transnacionales
                            </li>
                            <li>
                                • Docente de asignaturas a nivel
                                Licenciatura en distintas Universidades.
                            </li>
                            <li>
                                • Docente y Coach en programas de
                                formación COP de ILA Coaching y
                                Consultoria SC.
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 section-reponsive col-sm-6">
                        <div class="content">
                            <img src="{{ asset('img/team_3.png') }}" alt="">
                            <h6 class="name">Catalina Sánchez Coppe</h6>
                            <p class="position">Consultora Asociada</p>
                        </div>
                        <ul>
                            <li>
                                • Coach Ontológico, certificado por
                                Newfield Consulting Mexico
                            </li>
                            <li>
                                • Bachelor of Arts in Media and Cultural
                                Studies, Macquarie University, Sydney
                                Australia.
                            </li>
                            <li>
                                • Diplomado en Desarrollo Humano del
                                Instituto Éxito Humano, donde
                                desarrolló también funciones como
                                coach, conferencista, logística de
                                seminarios y ventas.
                            </li>
                            <li>
                                • Experiencia en Capacitación y Recursos
                                Humanos dentro de algunas empresas,
                                en las que ha apoyado y creado modelos
                                de valorización y selección de personal.
                            </li>
                            <li>
                                • Coaching y mentoring personal.
                            </li>
                            <li>
                                • Consultora y facilitadora Organizacional.
                            </li>
                            <li>
                                • Actriz profesional.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('scripts')
    <script>
        var token = $('#tokenID').attr('content');
        $( '#infoForm' ).submit(function ( e ) {
            e.preventDefault();
            var form = $(this);
            var fd = new FormData(this);
            var url = form.attr('action');

            $.ajax({
                url: url,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: fd,
                contentType:false,
                processData: false,
                success: function(response){
                    if(response.success){
                        $('#alert-contact').removeClass('hidden');
                        form.trigger("reset");
                    }
                }
            });

        });

    </script>
@endsection