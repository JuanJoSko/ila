@extends('layouts.app')

@section('content')
    <div class="wrap-slider fix-blog-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <div class="page-content">
                            <h2>Blog</h2>
                        </div>
                        <div class="breadcrumbs">
                            <ul>
                            <li><a href="{{ route('home') }}">Home</a></li>
                                <li>Blog</li>
                            </ul>
                        </div><!-- breadcrumb -->
                    </div><!-- page-title -->
                </div>
            </div>
        </div><!-- container -->
    </div><!-- wrap-slider -->


    <section class="flat-row v1 blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 ">
                        <div class="post-wrap">
                            <div class="wrap-entry">
                                <article class="post clearfix">
                                    <div class="entry">
                                        <div class="header-post">
                                            <ul class="post-comment">
                                                <li class="date">
                                                    <span class="day">13</span>        
                                                </li>
                                                <li class="comment">
                                                    DIC
                                                </li>
                                            </ul>
                                            <div class="flat-title">
                                            <h3 class="title-post"><a href="{{ route('entry0') }}">El rol de la mujer en la sobrevivencia humana</a></h3>                       
                                                <ul class="meta-post clearfix">
                                                    <li class="day"> 
                                                        <span>Publicado 13.12.2019</span>
                                                    </li>
                                                    <li class="author">
                                                        <a href="#">by Ila Coaching</a>
                                                    </li>
                                                </ul><!-- /.meta-post -->
                                            </div>      
                                        </div>
                                        <div class="featured-post">
                                            <div class="video-post">
                                                <div class="flat-control">
                                                    <img src="{{ asset('img/exp_4.jpg') }}" alt="image">

                                                    </a>
                                                </div>
                                            </div>
                                        </div><!-- /.feature-post -->
                                        <div class="content-post">
                                            <div class="text">
                                                <p class="note">En el último tiempo hemos accedido a cambios fundamentales en relación a la percepción del ser “mujer” en el mundo. Cambios paradigmáticos en relación al  mal llamado “sexo débil” son fundamentales para hacer frente al entorno cada vez más amenazante para la supervivencia humana. Si la mujer tiene un lugar biológico primordial en la existencia. ¿Cómo pensar algo disto en la sobrevivencia?...</p> 
                                            </div>
                                            <div class="flat-more">
                                                <div class="readmore">
                                                    <a href="{{ route('entry0') }}" class="flat-link" data-hover="CONTINUAR LEYENDO">CONTINUAR LEYENDO</a>
                                                </div>
                                                <div class="more-social">
                                                    <ul class="meta-vote">
                                                        <li class="vote"><a href="{{ route('entry0') }}" >0</a></li>
                                                    </ul>
                                                </div>
                                            </div>      
                                        </div>
                                    </div>    
                                </article>
                                <div class="divider h60">
                                </div>  
                                <article class="post style2 clearfix">
                                    <div class="entry">
                                        <div class="featured-post">
                                            <div class="video-post">
                                                <div class="flat-control">
                                                    <img src="{{ asset('img/esc_fe.jpg') }}" alt="image">
                                                   
                                                </div>
                                            </div>
                                        </div><!-- /.feature-post -->
                                        <div class="flat-entry">
                                            <div class="divider h40">
                                                
                                            </div>
                                            <div class="header-post">
                                                <ul class="post-comment">
                                                    <li class="date">
                                                        <span class="day">12</span>        
                                                    </li>
                                                    <li class="comment">
                                                        DIC
                                                    </li>
                                                </ul>
                                                <div class="flat-title">
                                                    <h3 class="title-post"><a href="{{ route('entry1') }}">Neurogénero y Liderazgo</a></h3>                       
                                                    <ul class="meta-post clearfix">
                                                        <li class="day"> 
                                                            <span>Publicado 12.12.2019</span>
                                                        </li>
                                                        <li class="author">
                                                            <a href="#">by Ila Coaching</a>
                                                        </li>
                                                    </ul><!-- /.meta-post -->
                                                </div>      
                                            </div>
                                            <div class="content-post">
                                                <div class="text">
                                                    <p class="note">El concepto de liderazgo ha sido ampliamente estudiado, clasificado y caracterizado. Tiene además una larga vida y una evolución constante en su definición...</p> 
                                                </div>
                                                <div class="flat-more">
                                                    <div class="readmore">
                                                        <a href="{{ route('entry1') }}" class="flat-link" data-hover="CONTINUAR LEYENDO">CONTINUAR LEYENDO</a>
                                                    </div>
                                                    <div class="more-social">
                                                        <ul class="meta-vote">
                                                            <li class="vote"><a href="{{ route('entry1') }}" >0</a></li>
                                                        </ul>
                                                    </div>
                                                </div>      
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divider h50">
                                    </div>    
                                </article>
                                <div class="divider h49">
                                </div>  
                            </div>
                            <div class="divider h41">
                                
                            </div>
                            {{-- <div class="blog-pagination">
                                <ul class="flat-pagination">
                                    <li class="back"><button class="flat-button bg-gray">NEWER POSTS</button></li>
                                    <li class="next"><button class="flat-button bg-gray">OLDER POSTS</button></li>
                                </ul><!-- /.flat-pagination -->
                            </div> --}}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="sidebar">
                            {{-- <div class="widget widget-recent-new">
                                <h4 class="widget-title">Recent News</h4>
                                <ul class="recent-new">
                                    <li><a href="#">In order to succeed, we must first believe that we can.</a>
                                    <br><span class="date-time">October 7, 2017</span>
                                    </li>
                                    <li><a href="#">The way to get started is to quit talking and begin doing.</a>
                                    <br><span class="date-time">October 7, 2017</span>
                                    </li>
                                    <li><a href="#">In order to succeed, we must first believe that we can.</a>
                                    <br><span class="date-time">October 7, 2017</span>
                                    </li>
                                    <li><a href="#">The way to get started is to quit talking and begin doing.</a>
                                    <br><span class="date-time">October 7, 2017</span>
                                    </li>
                                </ul>
                            </div> --}}
                            {{-- <div class="widget widget-categories">
                                <h4 class="widget-title">Categories</h4>
                                <ul class="cotegories">
                                    <li><a href="#">Investments</a></li>
                                    <li><a href="#">Budgets</a></li>
                                    <li><a href="#">Accounting</a></li>
                                    <li><a href="#">Portfolios</a></li>
                                    <li><a href="#">Wealth</a></li>
                                </ul>
                            </div> --}}

                            {{-- <div class="widget widget-review">
                                <div class="box-review face-review">
                                    <div class="box-header">
                                        <div class="box-icon">
                                            <a href="#"><i class="fa fa-facebook"></i></a>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <div class="info-review">
                                            <span>4.000</span>
                                        </div>
                                        <p>Fans</p>
                                    </div>
                                </div>
                                <div class="box-review twitter-review">
                                    <div class="box-header">
                                        <div class="box-icon">
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <div class="info-review">
                                            <span>3.000</span>
                                        </div>
                                        <p>Followers</p>
                                    </div>
                                </div>
                                <div class="box-review youtube-review">
                                    <div class="box-header">
                                        <div class="box-icon">
                                            <a href="#"><i class="fa fa-youtube-play"></i></a>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <div class="info-review">
                                            <span>2.000</span>
                                        </div>
                                        <p>Subscribers</p>
                                    </div>
                                </div>
                            </div> --}}

                            {{-- <div class="widget widget-store">
                                <div class="book-store">
                                    <h5 class="title">Book Store</h5>
                                    <p>Buy Book Online Now!</p>
                                    <button class="flat-button">Visit store</button>
                                </div>
                            </div>
                            <div class="widget widget-tags">
                                <h4 class="widget-title">Popular Tags</h4>
                                <div class="tags-list">
                                    <a class="tags-1" href="#">Creative</a>
                                    <a class="tags-2 none" href="#">Unique</a>
                                    <a class="tags-3" href="#">Photography</a>
                                    <a class="tags-4 none" href="#">Music</a>
                                    <a class="tags-5" href="#">Wordpress Template</a>
                                    <a class="tags-6 none" href="#">Ideas</a>
                                </div>
                            </div> --}}
                           
                            <div class="widget widget-fanspage">
                                <div id="fb-root"></div>
                                <h4 class="widget-title">Facebook</h4>

                                <div class="img-fans">
                                    <div class="fb-page" data-href="https://www.facebook.com/ilacoaching" data-tabs="timeline" data-height="380" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook/">Facebook</a></blockquote></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
@endsection