@extends('layouts.app')

@section('content')
    <div id="services-section" class="site-blocks-cover overlay aos-init aos-animate" style="background-image: url({{ asset('/img/contacto.jpg') }});" data-aos="fade">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-lg-5 ml-auto text-left align-self-center align-self-md-center fix-aling-center">
                    <div class="col-md-6">
                        
                    </div>
                    <div class="col-md-6 form-content">
                        <div class="divider "></div>
                        <h5 class="title-info title-red mb-1">CONTÁCTANOS</h5>
                        <p id="alert-contact" class="alert alert-success hidden">Tu mensaje ha sido enviado. <br>Gracias por ponerte en contacto con nosotros.</p>
                        <form id="contactForm" class="form-contact" method="POST" action="{{ route('contactMail') }}">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input placeholder="Nombre completo" type="text" class="form-control" name="name" required="" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input placeholder="Teléfono" type="text" class="form-control" name="phone" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input placeholder="Correo electónico" type="email" class="form-control" name="email" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <textarea placeholder="Mensaje" class="form-control" name="msg" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-12 offset-md-4">
                                    <button class="btn flat-button" type="submit">ENVIAR</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script>
        var token = $('#tokenID').attr('content');
        $( '#contactForm' ).submit(function ( e ) {
            e.preventDefault();
            var form = $(this);
            var fd = new FormData(this);
            var url = form.attr('action');

            $.ajax({
                url: url,
                headers: {'X-CSRF-TOKEN': token},
                type: 'POST',
                dataType: 'json',
                data: fd,
                contentType:false,
                processData: false,
                success: function(response){
                    if(response.success){
                        $('#alert-contact').removeClass('hidden');
                        form.trigger("reset");
                    }
                }
            });

        });

    </script>
@endsection