@extends('layouts.app')

@section('content')
    <div class="wrap-slider fix-blog-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <div class="page-content">
                            <h2>El rol de la mujer en la sobrevivencia humana</h2>
                        </div>
                        <div class="breadcrumbs">
                            <ul>
                            <li><a href="{{ route('home') }}">Home</a></li>
                                <li>Blog</li>
                            </ul>
                        </div><!-- breadcrumb -->
                    </div><!-- page-title -->
                </div>
            </div>
        </div><!-- container -->
    </div><!-- wrap-slider -->
    <section class="flat-row v1 blog-detais">
    <div class="container">
        <div class="row">
            <div class="col-md-9 ">
                <div class="post-wrap">
                    <div class="wrap-social">
                        <div class="divider h115">
                        </div>
                        <ul class="social-links style2">
                            <li class="face"><a href="https://www.facebook.com/ilacoaching"><i class="fa fa-facebook"></i></a></li>
                            <li class="instagram"><a href="https://www.facebook.com/ilacoaching"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    <div class="wrap-entry">
                        <article class="post clearfix">
                            <div class="entry">
                                <div class="header-post">
                                    <ul class="post-comment">
                                        <li class="date">
                                            <span class="day">04</span>        
                                        </li>
                                        <li class="comment">
                                            DIC
                                        </li>
                                    </ul>
                                    <div class="flat-title">
                                        <h3 class="title-post"><a href="blog-details.html">El rol de la mujer en la sobrevivencia humana</a></h3>                       
                                        <ul class="meta-post clearfix">
                                            <li class="day"> 
                                                <span>Publicado 13.12.2019</span>
                                            </li>
                                            <li class="author">
                                                <a href="#">by Ila Coaching</a>
                                            </li>
                                        </ul><!-- /.meta-post -->
                                    </div>      
                                </div>
                                <div class="featured-post">
                                    <div class="flat-control">
                                    <img src="{{ asset('img/exp_4.jpg') }}" alt="image">
                                    </div>
                                </div><!-- /.feature-post -->
                                <div class="note-post">
                                    <div class="text">
                                        <p>En el último tiempo hemos accedido a cambios fundamentales en relación a la percepción del ser “mujer” en el mundo. Cambios paradigmáticos en relación al  mal llamado “sexo débil” son fundamentales para hacer frente al entorno cada vez más amenazante para la supervivencia humana. Si la mujer tiene un lugar biológico primordial en la existencia. ¿Cómo pensar algo disto en la sobrevivencia?
                                        </p> 
                                    </div>
                                </div>
                                <div class="content-post">
                                    <p class="text-p">No existe vida sin una mujer. Encarna en su propio cuerpo el entorno primario y necesario para la sobrevivencia. Todos venimos de una mujer.
                                        Como dicta nuestra filosofía, En ILA Coaching y Consultoría creemos en la necesidad de construir un mundo participativo e incluyente, basado en el respeto y el empoderamiento estratégico de las diferencias individuales y colectivas. Dejar de invalidar las diferencias de cualquier tipo (Género, raza, cultura, etc) para comenzar a validarlas y sobretodo aprovecharlas. Creemos que el mundo ha tratado de eliminar el disonante, al distinto, al disidente. La sobrevivencia depende de integrarlo y aprovecharlo.
                                        Puede que la palabra aprovechar pueda ser malentendida.
                                        No estamos hablando de “aprovecharse” de la diferencia, estamos hablando de aprovechar la diferencia como fuente de innovación , empoderamiento y sobrevivencia.
                                        
                                        </p>
                                </div>
                                <div class="content-post content-post2">
                                    <p>Nuestra propuesta del coaching psicogénico está orientado a la necesidad de aperturas individuales y colectivas hacia la transformación. En esta apertura creemos la mujer tiene un rol fundamental. ¿Quién mejor que la mujer para facilitar este proceso de resiliencia y transformación que necesita la especie humana?
                                    </p>
                                    <p>Lamentablemente hemos puesto a la mujer hace muchos siglos en un espacio de invalidación por diferencia de la cual ha resistido airosa, empoderada y fortalecida. Aprovechemos entonces ese error humano. Aprovechemos ahora esa injusticia cometida. No nos vayamos al otro lado y hagamos lo mismo hoy con el hombre. Aprovechemos esta experiencia femenina en la evolución de la humanidad y pidámosle que compartiendo su aprendizaje sea hoy la facilitadora central de estas transformaciones.

                                    </p>
                                   
                                </div>
                                <div class="content-post content-post2">
                                    <h4 class="title-entry">¿No les parece?</h4>
                                </div>
                            </div>    
                        </article>
                        <div class="divider h47">
                        </div>    
                        <div class="main-single">
                            <div class="comments-area">
                                <h2 class="comments-title">0 Comentarios</h2>
                                <ol class="comment-list">
                                </ol><!-- .comment-list -->

                                <div class="comment-respond" id="respond">
                                    <h2 class="comment-reply-title">Deja un comentario</h2>
                                    <form novalidate="" class="comment-form" id="commentform" method="post" action="#">
                                        <p class="comment-notes">                                      
                                            <input type="text" placeholder="Nombre" aria-required="true" size="30" value="" name="author" id="author">
                                        </p>
                                        <p class="comment-form-email">          
                                            <input type="email" placeholder="Correo" size="30" value="" name="email" id="email">
                                        </p>  
                                        <p class="comment-form-url">          
                                            <input type="url" placeholder="Website" size="30" value="" name="url" id="url">
                                        </p>                                  
                                        <p class="comment-form-comment">
                                            <textarea class="comment-messages" tabindex="4" placeholder="Mensaje" name="comment" required></textarea>
                                        </p>                        
                                        <p class="form-submit">                 
                                            <button class="flat-button font-poppins">Enviar</button>
                                        </p>
                                    </form>
                                </div><!-- /.comment-respond -->
                            </div><!-- /.comments-area -->
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sidebar">
                    <div class="widget widget-recent-new">
                        <h4 class="widget-title">Post Recientes</h4>
                        <ul class="recent-new">
                        <li><a href="{{ route('entry1') }}">Neurogénero y Liderazgo</a>
                            <br><span class="date-time">Diciembre 12, 2019</span>
                            </li>
                           
                        </ul>
                    </div>
                   
                    <div class="widget widget-fanspage">
                        <div id="fb-root"></div>
                        <h4 class="widget-title">Facebook</h4>

                        <div class="img-fans">
                            <div class="fb-page" data-href="https://www.facebook.com/ilacoaching" data-tabs="timeline" data-height="380" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook/">Facebook</a></blockquote></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
