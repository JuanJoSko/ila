@extends('layouts.app')

@section('content')
    <div class="wrap-slider fix-blog-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <div class="page-content">
                            <h2>Neurogénero y Liderazgo</h2>
                        </div>
                        <div class="breadcrumbs">
                            <ul>
                            <li><a href="{{ route('home') }}">Home</a></li>
                                <li>Blog</li>
                            </ul>
                        </div><!-- breadcrumb -->
                    </div><!-- page-title -->
                </div>
            </div>
        </div><!-- container -->
    </div><!-- wrap-slider -->
    <section class="flat-row v1 blog-detais">
    <div class="container">
        <div class="row">
            <div class="col-md-9 ">
                <div class="post-wrap">
                    <div class="wrap-social">
                        <div class="divider h115">
                        </div>
                        <ul class="social-links style2">
                            <li class="face"><a href="https://www.facebook.com/ilacoaching"><i class="fa fa-facebook"></i></a></li>
                            <li class="instagram"><a href="https://www.facebook.com/ilacoaching"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                    <div class="wrap-entry">
                        <article class="post clearfix">
                            <div class="entry">
                                <div class="header-post">
                                    <ul class="post-comment">
                                        <li class="date">
                                            <span class="day">04</span>        
                                        </li>
                                        <li class="comment">
                                            DIC
                                        </li>
                                    </ul>
                                    <div class="flat-title">
                                        <h3 class="title-post"><a href="blog-details.html">Neurogénero y Liderazgo</a></h3>                       
                                        <ul class="meta-post clearfix">
                                            <li class="day"> 
                                                <span>Publicado 13.12.2019</span>
                                            </li>
                                            <li class="author">
                                                <a href="#">by Ila Coaching</a>
                                            </li>
                                        </ul><!-- /.meta-post -->
                                    </div>      
                                </div>
                                <div class="featured-post">
                                    <div class="flat-control">
                                    <img src="{{ asset('img/exp_2.jpg') }}" alt="image">
                                    </div>
                                </div><!-- /.feature-post -->
                                
                                <div class="content-post content-post2">
                                    <p><span style="font-weight: 400;">El concepto de liderazgo ha sido ampliamente estudiado, clasificado y caracterizado. Tiene adem&aacute;s una larga vida y una evoluci&oacute;n constante en su definici&oacute;n.</span></p>
<p><span style="font-weight: 400;">En la actualidad hemos tenido un giro hacia la biolog&iacute;a con el fin de encontrar &ldquo;certezas&rdquo; y f&oacute;rmulas f&aacute;cticas. Hemos girado hacia la biolog&iacute;a. Espec&iacute;ficamente hacia la neurociencia aplicada al liderazgo, o mejor dicho, al Neuromanagement.&nbsp;</span></p>
<p><span style="font-weight: 400;">La capacidad cerebral aplicada al potencial de liderazgo depende de un conjunto de factores que se agrupan en: anat&oacute;micos, gen&eacute;ticos, ambientales, sociales y emocionales. A su vez debemos tener en cuenta tambi&eacute;n el fen&oacute;meno de la neuroplasticidad cerebral o el proceso por el que el aprendizaje y la experiencia van modificando nuestro cerebro a lo largo de la vida. S&iacute;! Es cierto. A&uacute;n en nuestros &uacute;ltimos minutos de agon&iacute;a, nuestro cerebro seguir&aacute; cambiando.</span></p>
<p><span style="font-weight: 400;">Sin embargo, debido a la plasticidad neuronal no es posible configurar un &uacute;nico patr&oacute;n de explicaci&oacute;n para hombres y mujeres en su relaci&oacute;n con el liderazgo. La influencia de las diferencias neurobiol&oacute;gicas nos permite hablar de la contribuci&oacute;n hormonal y de la materia (gris y blanca) en los estilos de liderazgo.</span></p>
<p><span style="font-weight: 400;">A esto lo conocemos como neurog&eacute;nero y buscamos su importancia en el liderazgo.</span></p>
<p><span style="font-weight: 400;">En relaci&oacute;n al sistema endocrino (hormonal), recordemos que varones y hembras segregan andr&oacute;genos y estr&oacute;genos, pero lo hacen en diferentes cantidades, por lo que predomina un tipo sobre el otro en cada sexo.</span></p>
<p><span style="font-weight: 400;">La influencia de las hormonas determina el mapa cerebral, mayor nivel de andr&oacute;genos como la testosterona, desarrolla m&aacute;s el hemisferio derecho, y el pensamiento lineal y sist&eacute;mico, potenciando habilidades en las que punt&uacute;an m&aacute;s altamente los varones, como visuoespaciales o geom&eacute;tricas.&nbsp;</span></p>
<p><span style="font-weight: 400;">Por otra parte, la influencia de un mayor nivel de estr&oacute;genos convierten cada estado hormonal de la mujer en un impulso de diferentes conexiones neurobiol&oacute;gicas, procesando nuevos sentimientos, intereses y emociones. Exacerbando de este modo, las tareas de conexi&oacute;n y diversidad de variables.</span></p>
<p><span style="font-weight: 400;">En relaci&oacute;n con la materia cerebral (gris y blanca), la &ldquo;materia gris&rdquo; tiene que ver con los centros de procesamiento de la informaci&oacute;n, mientras que la materia blanca representa la uni&oacute;n entre los centros de procesamiento. La primera entonces tiene que ver con la densidad de atenci&oacute;n en un proceso o tarea. La segunda con una mayor posibilidad de conexiones o tareas al mismo tiempo.</span></p>
<p><span style="font-weight: 400;">Neurofisiol&oacute;gicamente hablando, los machos tienen 6,5 veces m&aacute;s materia gris que las hembras, mientras que la cantidad de materia blanca es 10 veces superior en ellas que en ellos.</span></p>
<p><span style="font-weight: 400;">De esta facticidad biol&oacute;gica, se desprenden las siguientes aseveraciones:</span></p>
<p><span style="font-weight: 400;">- La densidad de atenci&oacute;n ser&aacute; m&aacute;s concreta y concentrada en los varones, y un poco m&aacute;s difusa aunque m&aacute;s ancha (abarcar&aacute; m&aacute;s elementos) en las mujeres. Es decir si nuestro objetivo al nombrar a un l&iacute;der es enfatizar la profundidad y calidad de una labor, probablemente ser&aacute; m&aacute;s efectivo un hombre. Si nuestro objetivo, al contrario, es buscar un l&iacute;der que coordine mayor diversidad de &aacute;reas y/o procesos, probablemente ser&aacute; m&aacute;s efectiva una mujer.</span></p>
<p><span style="font-weight: 400;">- El cerebro femenino tiene mayor capacidad para optimizar procesos neurosensoriales o perceptivos.</span></p>
<p><span style="font-weight: 400;">- La memoria gen&eacute;tica de la hembra es mayor que la del var&oacute;n.</span></p>
<p><span style="font-weight: 400;">- Las mujeres tienden a utilizar los dos hemisferios cerebrales de forma &iacute;ntegra en tareas mentales complejas. Entonces al liderar buscar&aacute;n soluciones de diversos matices (l&oacute;gicas, emocionales, creativas, racionales, Etc.)</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">- La memoria emocional es m&aacute;s influyente en los procesos de toma de decisiones en el cerebro femenino.</span></p>
<p><span style="font-weight: 400;">- Las zonas cerebrales relacionadas con la agresi&oacute;n son mayores en el cerebro masculino.</span></p>
<p><span style="font-weight: 400;">- Las capacidades innatas de empat&iacute;a y comunicaci&oacute;n son mayores en las mujeres.</span></p>
<p>&nbsp;</p>
<p><span style="font-weight: 400;">-Se adjudica al var&oacute;n una mayor capacidad visuoespacial, memoria espacial, rotaci&oacute;n mental de im&aacute;genes, resoluci&oacute;n de problemas matem&aacute;ticos, pero tambi&eacute;n mayor agresividad y mejores aptitudes de precisi&oacute;n manual y corporal.</span></p>
<p><span style="font-weight: 400;">-A la mujer por el contrario se le adjudica una mejor fluidez verbal, tareas motoras delicadas, localizaci&oacute;n de objetos en una serie, c&aacute;lculo, sensibilidad, percepci&oacute;n de niveles bajos de estimulaci&oacute;n en sentidos, por poner algunos ejemplos.</span></p>
<p><span style="font-weight: 400;">De este modo, el cerebro femenino tendr&aacute; gen&eacute;ticamente mejores circuitos para la empat&iacute;a, mientras que el del hombre estar&aacute; mejor preconcebido para analizar, explorar y construir sistemas.</span></p>
<p><span style="font-weight: 400;">El nuevo liderazgo en organizaciones, negocios y otros &aacute;mbitos sociales y pol&iacute;ticos, tiene gran inter&eacute;s en la inserci&oacute;n social de los sentimientos, algo para lo que la mujer est&aacute; especialmente dotada.&nbsp;</span></p>
<p><span style="font-weight: 400;">Para una buena toma de decisiones se requiere la funci&oacute;n arm&oacute;nica de tres aspectos muy equilibrados en el desarrollo neural de la mujer: autoobservaci&oacute;n, razonamiento y gesti&oacute;n de las emociones.</span></p>
<p><span style="font-weight: 400;">La invitaci&oacute;n no es a pensar que un cerebro es mejor que el otro. La invitaci&oacute;n es a aprovechar estrat&eacute;gicamente las diferencias cerebrales o de neurog&eacute;nero para impactar en la efectividad y desempe&ntilde;o de las organizaciones. Y, desde la neuroplasticidad neuronal, desafiar a hombres y mujeres a integrar y aprender aquellas tareas y/o funciones que son m&aacute;s afines al sexo opuesto.</span></p>
                                </div>
                            </div>    
                        </article>
                        <div class="divider h47">
                        </div>    
                        <div class="main-single">
                            <div class="comments-area">
                                <h2 class="comments-title">0 Comentarios</h2>
                                <ol class="comment-list">
                                </ol><!-- .comment-list -->

                                <div class="comment-respond" id="respond">
                                    <h2 class="comment-reply-title">Deja un comentario</h2>
                                    <form novalidate="" class="comment-form" id="commentform" method="post" action="#">
                                        <p class="comment-notes">                                      
                                            <input type="text" placeholder="Nombre" aria-required="true" size="30" value="" name="author" id="author">
                                        </p>
                                        <p class="comment-form-email">          
                                            <input type="email" placeholder="Correo" size="30" value="" name="email" id="email">
                                        </p>  
                                        <p class="comment-form-url">          
                                            <input type="url" placeholder="Website" size="30" value="" name="url" id="url">
                                        </p>                                  
                                        <p class="comment-form-comment">
                                            <textarea class="comment-messages" tabindex="4" placeholder="Mensaje" name="comment" required></textarea>
                                        </p>                        
                                        <p class="form-submit">                 
                                            <button class="flat-button font-poppins">Enviar</button>
                                        </p>
                                    </form>
                                </div><!-- /.comment-respond -->
                            </div><!-- /.comments-area -->
                        </div> 
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="sidebar">
                    <div class="widget widget-recent-new">
                        <h4 class="widget-title">Post Recientes</h4>
                        <ul class="recent-new">
                        <li><a href="{{ route('entry0') }}">El rol de la mujer en la sobrevivencia humana</a>
                            <br><span class="date-time">Diciembre 13, 2019</span>
                            </li>
                           
                        </ul>
                    </div>
                   
                    <div class="widget widget-fanspage">
                        <div id="fb-root"></div>
                        <h4 class="widget-title">Facebook</h4>

                        <div class="img-fans">
                            <div class="fb-page" data-href="https://www.facebook.com/ilacoaching" data-tabs="timeline" data-height="380" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/facebook/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/facebook/">Facebook</a></blockquote></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
