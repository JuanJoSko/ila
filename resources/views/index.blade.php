@extends('layouts.app')

@section('content')
    @include('dynamic.slider')

    <section class="flat-row v19 home1">
        <div class="container">
            <div class="row style-ove">
                <div class="col-md-7 section-reponsive col-sm-6">
                    <div class="divider h50">
                    </div>
                    <div class="profile">
                        <h6>
                            Somos una empresa innovadora, experta en coaching, consultoría
                            y capacitación. Queremos dar a personas y organizaciones, una
                            formación calificada y un camino de transformación que les
                            permita impactar positivamente en su desempeño, organizaciones
                            y en su vida.
                        </h6>
                        <div class="pro-content">
                            <h5 class="title-info">Ofrecemos</h5>
                            <ul>
                                <li>
                                    <p>
                                        ● Actuar desde una concepción del ser humano basada en el cambio
                                        y en el respeto por la diferencia.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        ● Desarrollar y afianzar en todos los participantes los contenidos
                                        del Programa.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        ● Utilizar el modelo de coaching ontológico como columna vertebral
                                        del programa, enriquecido con herramientas psicodinámicas que
                                        permiten
                                        su aplicación concreta tanto de manera individual como en las
                                        organizaciones.
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        ● Enriquecer la formación del participante desde la especialización en
                                            áreas especificas.
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 section-reponsive col-sm-6">
                    <div class="feature-about-us">
                        <img src="{{ asset('/img/home_2.jpg') }}">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="row v12 sec-book bg-gray">
        <div class="container">
            <div class="col-md-6">
                <img src="{{ asset('img/map.png') }}" alt="">
            </div>
            <div class="col-md-6">
                <div class="pro-content fix-top-10">
                    <h5 class="title-info">Misión</h5>
                    <p>
                        Diseñamos y ejecutamos programas de formación,
                        procesos de intervención y experiencias de aprendizaje
                        dando respuesta a los objetivos, necesidades e
                        inquietudes de nuestros clientes reconociendo en ellos
                        la experticia en la actividad que desarrollan.
                    </p>
                </div>
                <div class="pro-content">
                    <h5 class="title-info">Vision</h5>
                    <p>
                        Ser un equipo profesional que integra diversidad de
                        miradas para generar intervenciones poderosas que
                        facilitan procesos de transformación, potencian el
                        desempeño organizacional y promueven el bienestar de
                        las personas.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section class="flat-row v20 bg-theme">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section center oregano">
                        <h5 class="title-info title-red mb-1">
                            Personalizamos nuestros cursos y consultorías
                            de acuerdo a las necesidades de cada cliente
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="blog-carousel">
                <div class="post-shortcode">
                    <article class="post style3 lates first clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="blog-details.html"><img src="{{asset('/img/consultoria.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="blog-details.html">Consultoría</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates second clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="blog-details.html">
                                    <img src="{{asset('/img/diplomados.jpg')}}" alt="image">
                                </a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="blog-details.html">Diplomados</a></h6>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates three clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="blog-details.html"><img src="{{asset('/img/cursos.jpg')}}" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="blog-details.html">Cursos especializados</a></h6>
                            </div>
                        </div> 
                    </article>
                </div>
            </div>
        </div>
    </section>
    <section class="flat-row v12 parallax parallax7 sec-book">
        <div class="container">
            <div class="col-md-12">
                <div class="title-section center color-white sub oregano">
                    <h1 class="title style2">VIVE UN PROCESO DE APRENDIZAJE TRANSFORMACIONAL</h1>
                </div>
            </div>
        </div>
    </section>
    {{-- <section class="flat-row v20 bg-theme">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section center oregano">
                        <h1 class="title">Entérate de lo nuevo</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="blog-carousel">
                <div class="post-shortcode">
                    <article class="post style3 lates first clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="blog-details.html"><img src="images/portfolio/px1.jpg" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="blog-details.html">How to Make Money as a Health Coach</a></h6>
                                <span class="notes">NUTRITION STRATEGIES</span>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates second clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="blog-details.html"><img src="images/portfolio/px2.jpg" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="blog-details.html">How to Make Money as a Health Coach</a></h6>
                                <span class="notes">NUTRITION STRATEGIES</span>
                            </div>
                        </div> 
                    </article>
                    <article class="post style3 lates three clearfix">
                        <div class="entry">
                            <div class="featured-post">
                                <a href="blog-details.html"><img src="images/portfolio/px3.jpg" alt="image"></a>
                            </div><!-- /.feature-post -->
                            <div class="entry-post">
                                <h6 class="entry-title"><a href="blog-details.html">How to Make Money as a Health Coach</a></h6>
                                <span class="notes">NUTRITION STRATEGIES</span>
                            </div>
                        </div> 
                    </article>
                </div>
            </div>
        </div>
    </section> --}}

@endsection