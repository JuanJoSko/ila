
<div id="popupInfo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title title-info" id="exampleModalCenterTitle">¡Cumple con la NORMA 35 <br> en tu empresa!</h5>
          <button id="closePopUp" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <p>-Definición general</p>
            <p>-Proceso de Implementación</p>
            <p>-Rúbricas de Medición</p>
            <p>-Protocolos de Prevención general</p>
            <div class="form-group row mb-1 mt-1">
                <div class="col-md-12 offset-md-4">
                    <a href="http://ila.loc/contact-us" class="btn flat-button flat-button-min  center">CONTACTANOS</a>
                </div>
            </div>
            <a id="dontShow" href="javascript:;" data-dismiss="modal">No volver a mostrar</a>

        </div>
       
      </div>
    </div>
  </div>