<div class="site-wrapper">
    <!-- Header -->            
    <header id="header" class="header clearfix">
        <div class="flat-header clearfix">
            <div class="container">
                <div class="row">                 
                    <div class="header-wrap clearfix">
                        <div class="col-md-3">
                            <div id="logo" class="logo">
                                <h1><a href="index.html" rel="home">
                                    <img  src="{{ asset('img/logo_menu.png') }}" alt="">
                                </a></h1>
                            </div><!-- /.logo -->
                            <div class="btn-menu">
                                <span></span>
                            </div><!-- //mobile menu button -->
                        </div>
                        <div class="col-md-9">
                            <div class="nav-wrap">                            
                                <nav id="mainnav" class="mainnav">
                                    <ul class="menu"> 
                                        <li>
                                            <a class="nav-link" href="{{ route('home') }}">Inicio</a>
                                        </li>
                                        <li><a class="nav-link" href="{{ route('experience') }}">Experiencia ILA</a></li>
                                        <li>
                                            <a class="nav-link" href="{{ route('school') }}">Escuela</a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="{{ route('consultancy') }}">Consultoría</a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="{{ route('blog') }}">Blog</a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="{{ route('contact') }}">Contacto</a>
                                        </li>    
                                    </ul><!-- /.menu -->
                                   
                                </nav><!-- /.mainnav -->
                            </div><!-- /.nav-wrap -->
                        </div>                      
                    </div><!-- /.header-inner -->                 
                </div><!-- /.row -->
            </div>
        </div>
    </header><!-- /.header -->
</div><!-- /.site-wrapper -->