<!-- Footer -->
<footer class="footer style2 color-green">
    <div class="container">
        <div class="row"> 
            <div class="col-md-4 col-sm-6">  
                <div class="widget widget-infomation left">
                        <a href="index.html">
                            <img  src="{{ asset('img/logo_footer.png') }}" alt="">
                        </a>
                    {{-- <ul class="flat-information">
                        <li class="phone"><a href="#">+1 800 666 6688</a></li>
                        <li class="email"><a href="#">info.yolo@gmail.com</a></li>
                        <li class="address"><a href="#" >40 Baria Sreet 133/2 NewYork City, US</a></li>
                    </ul>  --}}
                </div>         
            </div><!-- /.col-md-3 --> 
            <div class="col-md-4 col-sm-6">  
                <div class="widget widget-infomation">
                    <h4 class="widget-title">SERVICIOS</h4>
                    <ul class="one-half ">
                        <li><a href="{{ route('consultancy') }}">Consultoría</a></li>
                        <li><a href="#">Programas de formación organizacional</a></li>
                        <li><a href="#">Cursos abiertos</a></li>
                    </ul>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-4 col-sm-6">  
                <div class="widget widget-infomation center">
                    <h4 class="widget-title">CONTÁCTANOS</h4>
                    <ul class="social-links style2 center">
                        <li><a target="_blank" href="https://www.facebook.com/ilacoaching"><i class="fa fa-facebook"></i></a></li>
                        <li><a target="_blank" href="https://www.instagram.com/ilamexicocoaching"><i class="fa fa-instagram"></i></a></li>
                        <li><a target="_blank" href="https://web.whatsapp.com/send?phone=+5521868492"><i class="fa fa-whatsapp"></i></a></li>
                    </ul>        
                </div>         
            </div>
        </div><!-- /.row -->    
    </div><!-- /.container -->
</footer>
<div class="bottom color-green center">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-6 bottom-reponsive">
                <div class="copyright">
                    <p>Copyright © 2019 - Todos los derechos reservados a ILA®. Powered by <a class="link-diamond" href="https://mktdiamond.com/">MKTDiamond</a></p>                   
                </div>                 
            </div> 
        </div>                  
    </div><!-- /.container -->
</div>